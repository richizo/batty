# About

# Art License

* The japanese quick fox is designed by [Daryl Gray](http://monotonousg.deviantart.com/) and the original draw can be found here:
 * http://monotonousg.deviantart.com/art/Japanese-Fox-Design-516395921

* The battery icons used on the layout by [Dooder](http://www.freepik.com/dooder):
 * http://www.freepik.com/free-vector/bulbs-with-batteries-design_894375.htm

# Thanks

Projects used to help me with the code:

* https://github.com/Der-Schubi/battery-remaining-time

* https://github.com/milliburn/gnome-shell-extension-battery_status

# Contributing

* Open an issue

* Fork the repository

* Test your code

* Do a merge request

Let's go: [git@gitlab.com:richizo/batty.git](git@gitlab.com:richizo/batty.git)